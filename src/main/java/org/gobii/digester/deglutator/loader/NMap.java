package org.gobii.digester.deglutator.loader;

import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class NMap {

	private String referenceTable;
	private Map<String, String> referenceEquality;
	private String referenceColumn;
	private String valueColumn;

}
