package org.gobii.digester.deglutator.loader;

public interface Loader {

	void load(String s) throws Exception;

	void commit() throws Exception;
}
