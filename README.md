# Deglutator

**NOTE THIS IS A NON-WORKING MODULE**

#### Purpose

Consume the data streams from the gobii.masticater, directly load to a postgres temporary table, then transfer to the core table. Should resolve values from database in accordance to `nmap` definitions, and deduplicate columns in accordance to `dedup` definitions, both found in `resources/ifl_map`.

Additionally, should handle load of matrix data to HDF5.

#### Motivation

Currently, the digester in standalone is shelling out to both python code (ifl) and C code (hdf5). This slapdash polyglot implementation makes debuggin, deployment, and development all a nightmare. The motiviation of this library was to unify everything under a JVM language, in the case Java, to alleviate these issues and encapsulate everything under the same housing.

Furthermore, under the digester implementation, the file was streamed continuously from its origin to the database, with one movement in database from temporary file to final destination. Currently, there a number of IO calls as the file is manipulated by bash calls, multiple python calls, and I'm sure a kitchen sink or two.

A single stream implementation will _greatly_ increase performance, as disk IO calls are the least performant portion of any system.

#### Issues with this implementation

##### SQL Code

This repo was implemented as a proof of concept, which was arguably achieved, however at great cost. The SQL code to move everything from the temporary table to the final table is created by a mismash of `String.format` calls, and misguided patterns. If this is to be implemented further, it would be best to use a SQL templating library of some sort.

##### HDF5

HDF5 is a pain to work with in Java. I'm by no means saying that it's no performant, or well optimized for our needs, but rather that like the ifl code, it is written in a non-conforming language to the JVM. Arguably, this could be a good thing as C code can reach higher performance hurdles than java can, but the cost of code implementation may be higher than what is gained.

If this module is to be implemented, either HDF5 would need to be dropped for Postgres, or a better encapsulation of the HDF5 code than shell outs should be written. Consider using a native library implementation, or an HDF5 server implementation much like postgres.


God speed.
